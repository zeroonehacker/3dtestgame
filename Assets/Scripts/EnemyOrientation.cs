﻿using UnityEngine;
using System.Collections;

public class EnemyOrientation : MonoBehaviour {

    public float verticalVelocity = 0.0f;
    public float movementSpeed = 0.00005f;
    CharacterController character;
    SuitManHealth bodyHealth;

    GameObject manFood;
    public float RotationSpeed;
    private Quaternion _lookRotation;
    private Vector3 _direction;

    public float MAGAZINE_SIZE = 30f;
    public GameObject bullet_prefab;
    public float bulletSpeed = 2000.0f;
    public float bullets;
    public AudioSource fireSound, reloadSound;
    public bool isReloading = false;
    public float farDistance = 50f;

    // Use this for initialization
    void Start () {
        character = gameObject.GetComponent<CharacterController>();
        bodyHealth = gameObject.GetComponent<SuitManHealth>();


        MAGAZINE_SIZE = -Physics.gravity.y;
        bullets = MAGAZINE_SIZE;
        AudioSource[] audios = gameObject.GetComponents<AudioSource>();
        fireSound = audios[0];
        reloadSound = audios[1];

        manFood = GameObject.Find("/Player/Camera");
        Debug.Log(manFood);
        RotationSpeed = 2.0f;
    }
	
	// Update is called once per frame
	void Update () {
        faceManFood();
        fire();
    }

    void MoveForward(float forwardSpeed)
    {
        float sideSpeed = 0f;

        verticalVelocity += Physics.gravity.y * Time.deltaTime;
        Vector3 speed = new Vector3(sideSpeed * movementSpeed, verticalVelocity, forwardSpeed * movementSpeed);
        speed = transform.rotation * speed;
        character.Move(speed * Time.deltaTime);
    }

    void faceManFood()
    {
        _direction = (manFood.transform.position - transform.position).normalized;

        //create the rotation we need to be in to look at the target
        _lookRotation = Quaternion.LookRotation(_direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed*1000000f);
    }

    void fire()
    {
        float targetDistanceSqr = (transform.position - manFood.transform.position).magnitude;
        if (targetDistanceSqr<farDistance*farDistance && !reloadSound.isPlaying &&Time.frameCount%10==0)
        {
            if (bullets > 0)
            {
                GameObject theBullet = (GameObject)Instantiate(bullet_prefab, transform.position + 5 * transform.forward, transform.rotation);
                theBullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed);
                if (!fireSound.isPlaying)
                    fireSound.Play();
                bullets -= MAGAZINE_SIZE / 240f;
            }
            else
            {
                fireSound.Stop();
                isReloading = true;
                reload();
            }
        }
    }

    void reload()
    {
        if (!reloadSound.isPlaying && isReloading)
        {
            fireSound.Stop();
            reloadSound.Play();
            bullets = MAGAZINE_SIZE;
            isReloading = false;
        }
    }
}
