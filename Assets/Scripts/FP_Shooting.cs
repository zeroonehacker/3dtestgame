﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class FP_Shooting : MonoBehaviour {

    public float  MAGAZINE_SIZE=30f;
    public GameObject bullet_prefab;
    public float bulletSpeed = 10000.0f;
    public  float bullets;
    Camera cam;
    public AudioSource fireSound, reloadSound;
    public bool isReloading = false;
	// Use this for initialization
	void Start () {
        MAGAZINE_SIZE = -Physics.gravity.y;
        bullets = MAGAZINE_SIZE;
        cam = Camera.main;
        AudioSource[] audios = gameObject.GetComponents<AudioSource>();
        fireSound = audios[0];
        reloadSound = audios[1];
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1") && !reloadSound.isPlaying && Time.frameCount % 2 == 0)
        {
            if (bullets > 0)
            {
                GameObject theBullet = (GameObject)Instantiate(bullet_prefab, cam.transform.position + 3 * cam.transform.forward, cam.transform.rotation);
                theBullet.GetComponent<Rigidbody>().AddForce(cam.transform.forward * bulletSpeed);
                if (!fireSound.isPlaying)
                    fireSound.Play();
                bullets -= MAGAZINE_SIZE / 30;
            }
            else
            {
                fireSound.Stop();
                isReloading = true;
                reload();
            }
        }
        if (Input.GetButtonUp("Fire1") && !reloadSound.isPlaying)
        {
            if (fireSound.isPlaying)
            {
                fireSound.Stop();
            }
            reload();
        }
        if (Input.GetButton("Fire2") && !reloadSound.isPlaying)
        {
            isReloading = true;
            reload();
        }
    }

    void reload()
    {
        if (!reloadSound.isPlaying&&isReloading)
        {
            fireSound.Stop();
            reloadSound.Play();
            bullets = MAGAZINE_SIZE;
            isReloading = false;
        }
    }
}
