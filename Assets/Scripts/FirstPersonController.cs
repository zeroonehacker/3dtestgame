﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour {

    public float movementSpeed = 5.0f;
    public float mouseSensitivity = 4.0f;

    public float upDownRange = 60.0f;
    public float verticalRotation = 0.0f;

    public float verticalVelocity = 0.0f;
    public float jumpSpeed = 6.0f;

    float hRecoil = 0.0f, vRecoil = 0.0f;

    CharacterController character;
    FP_Shooting shooter;

	// Use this for initialization
	void Start () {
        //Cursor.visible = false;
        character = GetComponent<CharacterController>();
        shooter = GameObject.Find("Player").GetComponent<FP_Shooting>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Cancel")) // this gives you access to the back button
        {
            Application.Quit(); // this quits the game.
        }

        if (Input.GetButtonDown("Fire1")&&!shooter.reloadSound.isPlaying)
        {
            hRecoil -= Time.deltaTime * 0.02f * 500f;
            vRecoil += Time.deltaTime * 0.05f * 500f;
        }

        //Left Right Rotation
        float rotateLeftRight = (Input.GetAxis("Mouse X")+hRecoil)*mouseSensitivity;
        transform.Rotate(0, rotateLeftRight, 0);

        //Top Bottom Rotation
        verticalRotation -= (Input.GetAxis("Mouse Y")+vRecoil) * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

        //Movement
        float forwardSpeed = Input.GetAxis("Vertical");
        float sideSpeed = Input.GetAxis("Horizontal");

        verticalVelocity += Physics.gravity.y * Time.deltaTime;

        if (Input.GetButtonDown("Jump") && character.isGrounded)
        {
            verticalVelocity = jumpSpeed;
        }

        Vector3 speed = new Vector3(sideSpeed*movementSpeed, verticalVelocity, forwardSpeed*movementSpeed);
        speed = transform.rotation * speed;
        character.Move(speed*Time.deltaTime);

        if (Input.GetButtonUp("Fire1"))
        {
            hRecoil = 0.0f;
            vRecoil = 0.0f;
        }
    }
}
