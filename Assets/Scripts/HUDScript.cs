﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScript : MonoBehaviour {
    FP_Shooting shooter;
    PlayerStats stats;
    public Slider slider;
    public Text bulletsLeft,healthLeft;
    public Color fillColor;
	// Use this for initialization
	void Start () {
        Cursor.visible = false;
        slider = GameObject.Find("AmmoSlider").GetComponent<Slider>();
        bulletsLeft = GameObject.Find("BulletText").GetComponent<Text>();
        healthLeft = GameObject.Find("healthText").GetComponent<Text>();
        
        shooter = GameObject.Find("/Player").GetComponent<FP_Shooting>();
        stats = GameObject.Find("/Player").GetComponent<PlayerStats>();
    }
	
	// Update is called once per frame
	void Update () {
        slider.value = 30*shooter.bullets/shooter.MAGAZINE_SIZE;
        bulletsLeft.text =  ((int)30*shooter.bullets/shooter.MAGAZINE_SIZE).ToString();
        healthLeft.text = stats.health.ToString();

	}
}
