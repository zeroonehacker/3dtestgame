﻿using UnityEngine;
using System.Collections;
using System;

public class MuzzleFlash : MonoBehaviour {

    FP_Shooting shooter;

	// Use this for initialization
	void Start () {
        shooter = GameObject.Find("Player").GetComponent<FP_Shooting>();
        gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButton("Fire1")&&!shooter.reloadSound.isPlaying)
        {
            Flicker(true);
        }
        else
        {
            Flicker(false);
        }
	}

    void Flicker(bool State)
    {
        MeshRenderer[] renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach(MeshRenderer r in renderers)
        {
            if (State)
                r.enabled = RandBool();
            else
                r.enabled = State;
        }
    }

    bool RandBool()
    {
        System.Random gen = new System.Random();
        int prob = gen.Next(100);
        if (prob < 20)
            return true;
        else
            return false;
    }
}
