﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    public float health;
    bool damageVsible = false;
    bool alive;
	// Use this for initialization
	void Start () {
        alive = true;
    }
	
	// Update is called once per frame
	void Update () {

        if(Time.frameCount%40==0 && damageVsible)
        {
            damageVsible = false;
            GameObject.Find("DamageCanvas").GetComponent<Canvas>().enabled=false;
        }
        if (!alive)
        {
            Application.Quit();
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            GameObject.Find("DamageCanvas").GetComponent<Canvas>().enabled=true;
            damageVsible = true;
            health -= 1f;
            //Debug.Log(health);
        }
        if (health <= 0f)
        {
            alive = false;
        }
    }
}
