﻿using UnityEngine;
using System.Collections;

public class SuitManHealth : MonoBehaviour {

    public float health = 100.0f;
    public float deadBodyTime = 10.0f;
    float timeofDeath =-1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (health <= 0&&timeofDeath<0)
        {
            //Destroy(gameObject);
            timeofDeath = Time.time;
        }
        if (timeofDeath > 0 && Time.time>timeofDeath+deadBodyTime)
        {
            Destroy(gameObject);
        }

        //Debug.Log(health);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            health -= 10.0f;
        }
    }
}
