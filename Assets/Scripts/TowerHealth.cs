﻿using UnityEngine;
using System.Collections;

public class TowerHealth : MonoBehaviour {

    public float health = 100f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (health <= 0)
        {
            Destroy(transform.parent.gameObject);
        }
        if (Time.frameCount % 20 == 0&&gameObject.GetComponent<MeshRenderer>().enabled)
            gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            health -= 20.0f;
        }
        Debug.Log(transform.parent.gameObject + ":" + health);
    }
}
