﻿using UnityEngine;
using System.Collections;

public class bullet_impact : MonoBehaviour {

    float lifespan = 6.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        lifespan -= Time.deltaTime;
        if (lifespan <= 0)
            Explosion();
	}

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
        //Debug.Log(collision.gameObject);
    }

    void Explosion()
    {
        Destroy(gameObject);
    }
}
