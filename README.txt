Run 3dTest_v.0.02.exe to start the game..

Rules for the game:
===================
Shoot at the towers and avoid being shot at.

Controls:
=========
JUMP:Space
FORWARD: W
BACKWARD: S
RIGHT: D
LEFT: A
SHOOT: LMB
RELOAD: RMB
EXIT: Escape

HUD
===
Top right has health stats
Top left has current magazine stats
